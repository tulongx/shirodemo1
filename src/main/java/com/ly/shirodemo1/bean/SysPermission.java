package com.ly.shirodemo1.bean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author linyun
 * @date 2018/9/27 上午11:30
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SysPermission {

    private String id;
    private String uri;

}
