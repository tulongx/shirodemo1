package com.ly.shirodemo1.bean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Set;

/**
 * @author linyun
 * @date 2018/9/27 上午10:40
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class SysRole {

    private String id;
    private String name;

    /**
     * 包含的权限.
     */
    private Set<SysPermission> permissions;
}
