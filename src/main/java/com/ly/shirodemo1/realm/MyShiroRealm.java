package com.ly.shirodemo1.realm;

import com.ly.shirodemo1.bean.SysPermission;
import com.ly.shirodemo1.bean.SysRole;
import com.ly.shirodemo1.bean.SysUser;
import com.ly.shirodemo1.utils.DataSourceUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.authc.AuthenticationInfo;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.SimpleAuthenticationInfo;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.context.annotation.Configuration;

import java.util.HashSet;
import java.util.Set;

/**
 * @author linyun
 * @date 2018/9/27 上午10:39
 */
@Slf4j
@Configuration
public class MyShiroRealm extends AuthorizingRealm {


    /**
     * 此方法调用  hasRole,hasPermission的时候才会进行回调.
     * <p>
     * 权限信息.(授权):
     * 1、如果用户正常退出，缓存自动清空；
     * 2、如果用户非正常退出，缓存自动清空；
     * 3、如果我们修改了用户的权限，而用户不退出系统，修改的权限无法立即生效。
     * （需要手动编程进行实现；放在service进行调用）
     * 在权限修改后调用realm中的方法，realm已经由spring管理，所以从spring中获取realm实例，
     * 调用clearCached方法；
     * :Authorization 是授权访问控制，用于对用户进行的操作授权，证明该用户是否允许进行当前操作，如访问某个链接，某个资源文件等。
     *
     * @param principalCollection
     * @return
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        log.info("权限授权信息验证。。。");
        //获取用户
//        SysUser user = (SysUser) SecurityUtils.getSubject().getPrincipal();
        //获取登录用户名
        String username = (String) principalCollection.getPrimaryPrincipal();
        SimpleAuthorizationInfo authorizationInfo = new SimpleAuthorizationInfo();

        // 依据username从数据库中找到权限。
        Set<String> ps = new HashSet<>();
        SysUser user = DataSourceUtils.findOne(username);
        for (SysRole role : user.getRoles()) {
            authorizationInfo.addRole(role.getName());
            for (SysPermission permission : role.getPermissions()) {
                ps.add(permission.getUri());
            }
        }
        //添加 单个 权限
//        authorizationInfo.addStringPermission("admin:manage");
        //设置权限信息.
        authorizationInfo.setStringPermissions(ps);
        return authorizationInfo;
    }

    /**
     * 验证用户身份
     *
     * @param authenticationToken
     * @return
     * @throws AuthenticationException
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        log.info("用户登录验证。。。");
        if (authenticationToken.getPrincipal() == null) {
            return null;
        }
        //获取用户的输入的账号.
        String username = (String) authenticationToken.getPrincipal();
        //通过username从数据库中查找 User对象，如果找到，没找到.
        //实际项目中，这里可以根据实际情况做缓存，如果不做，Shiro自己也是有时间间隔机制，2分钟内不会重复执行该方法
        SysUser user = DataSourceUtils.findOne(username);

        //加密方式;
        //交给AuthenticatingRealm使用CredentialsMatcher进行密码匹配
        SimpleAuthenticationInfo authenticationInfo = new SimpleAuthenticationInfo(
                //用户名
                username,
                //密码
                user.getPwd(),
                //realm name
                getName()
        );
        return authenticationInfo;
    }

    public void clearCached() {
        PrincipalCollection principals = SecurityUtils.getSubject().getPrincipals();
        super.clearCache(principals);
    }

}
