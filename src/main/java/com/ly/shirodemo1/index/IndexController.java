package com.ly.shirodemo1.index;

import com.ly.shirodemo1.bean.SysUser;
import com.ly.shirodemo1.utils.DataSourceUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationToken;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Enumeration;

/**
 * @author linyun
 * @date 2018/9/27 上午11:48
 */
@Slf4j
@Controller
public class IndexController {

    @RequestMapping("/")
    public String index() {
        log.info("---首页---");
        return "index";
    }

    @RequestMapping("/login")
    public String login() {
        log.info("---登录页面---");
        return "login";
    }

    /**
     * 登录
     *
     * @param req
     * @return
     */
    @RequestMapping("/in")
    public String sginin(HttpServletRequest req) {
        Enumeration enu = req.getParameterNames();
        while (enu.hasMoreElements()) {
            String paraName = (String) enu.nextElement();
            System.out.println(paraName + ": " + req.getParameter(paraName));
        }
        String username = req.getParameter("username");
        String pwd = req.getParameter("pwd");

        Subject subject = SecurityUtils.getSubject();
        SecurityUtils.getSecurityManager().logout(subject);

        SysUser user = DataSourceUtils.findOne(username);
        if (user != null) {
            AuthenticationToken token = new UsernamePasswordToken(username, pwd);
            //进行验证，这里可以捕获异常，然后返回对应信息
            subject.login(token);
        }else{
            log.info("---用户不存在");
            return "redirect:/login";
        }
        return "/";
    }

    /**
     * 需要user权限
     *
     * @return
     */
    @RequestMapping("/user")
    public String user() {
        log.info("---user---");
        return "user";
    }

    /**
     * 需要admin权限.
     *
     * @return
     */
    @RequestMapping("/admin")
    public String admin() {
        log.info("---admin---");
        return "admin";
    }

    @RequestMapping("/test")
    public String test(){
        return "test";
    }


    @RequestMapping("/update")
    public String updatePermission(){

        return "成功更新";
    }

}
