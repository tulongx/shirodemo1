package com.ly.shirodemo1.utils;

import com.ly.shirodemo1.bean.SysPermission;
import com.ly.shirodemo1.bean.SysRole;
import com.ly.shirodemo1.bean.SysUser;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

/**
 * 模拟数据库
 *
 * @author linyun
 * @date 2018/9/27 上午11:03
 */
@Slf4j
public class DataSourceUtils {

    public static Map<String, SysUser> users = new HashMap<>();

    static {
        log.info("初始化数据.......");
        // 区分2ge

        SysRole roleUser = SysRole.builder().id(System.currentTimeMillis() + "").name("ROLE_USER").build();
        Set<SysRole> roles = new HashSet<>();
        SysRole roleAdmin = SysRole.builder().id(System.currentTimeMillis() + "").name("ROLE_ADMIN").build();

        SysPermission permission1 = SysPermission.builder().id(System.currentTimeMillis() + "").uri("/user").build();
        SysPermission permission2 = SysPermission.builder().id(System.currentTimeMillis() + "").uri("/admin").build();
        SysPermission permission3 = SysPermission.builder().id(System.currentTimeMillis() + "").uri("/test").build();

        Set<SysPermission> ps1 = new HashSet<>();
        ps1.add(permission1);

        Set<SysPermission> ps2 = new HashSet<>();
        ps2.add(permission2);

        roleUser.setPermissions(ps1);
        roleAdmin.setPermissions(ps2);

        // 第一个用户
        roles.add(roleUser);
        SysUser user1 = SysUser.builder()
                .id(System.currentTimeMillis() + "")
                .name("user1")
                // 123
                .pwd("202cb962ac59075b964b07152d234b70")
                .roles(roles)
                .build();
        users.put("user1", user1);

        // 第二个用户
        roles = new HashSet<>();
        roles.add(roleAdmin);
        SysUser user2 = SysUser.builder()
                .id(System.currentTimeMillis() + "")
                .name("user2")
                //123
                .pwd("202cb962ac59075b964b07152d234b70")
                .roles(roles)
                .build();
        users.put("user2", user2);
    }


    public static SysUser findOne(String username) {
        log.info("查询数据库");
        return users.get(username);
    }

    /**
     * 查询所有的权限
     *
     * @return
     */
    public static List<SysPermission> findAllPermission() {
        List<SysPermission> permissions = new ArrayList<>();
        SysPermission permission1 = SysPermission.builder().id(System.currentTimeMillis() + "")
                .uri("/test1").build();
        SysPermission permission2 = SysPermission.builder().id(System.currentTimeMillis() + "")
                .uri("/test2").build();
        SysPermission permission3 = SysPermission.builder().id(System.currentTimeMillis() + "")
                .uri("/test3").build();
        permissions.add(permission1);
        permissions.add(permission2);
        permissions.add(permission3);
        return permissions;
    }
}
